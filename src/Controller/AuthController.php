<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Repository\ApiRepository;
use App\Repository\UserRepository;

use App\Entity\User;
use DateTime;


class AuthController extends AbstractController
{
    private $session;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var ApiRepository
     */
    private $apiRepository;

    /**
     * ProductReviewController constructor.
     *
     * @param UserRepository $userRepository
     * @param ApiRepository $apiRepository
     */
    public function __construct(
        SessionInterface $session,
        UserRepository $userRepository,
        ApiRepository $apiRepository
    )
    {
        $this->session = $session;
        $this->userRepository = $userRepository;
        $this->apiRepository = $apiRepository;
    }

    /**
     * @Route("/auth", name="auth")
     */
    public function index()
    {   
        $message_type = $this->session->get('message_type');
        $message_type = $this->session->remove('message_type');
        return $this->render('order/index.html.twig', [
            'controller_name' => 'OrderController',
            'message_type' => $message_type,
        ]);
    }

    /**
     * @Route("/auth/login", name="auth_login")
     */
    public function login()
    {
        return $this->render('order/login.html.twig', [
            'controller_name' => 'OrderController',
        ]);
    }

    /**
     * @Route("/auth/loginAction", methods={"POST"}, name="auth_login_action")
     */
    public function loginAction(Request $request)
    {
        $message_type = null;
        if($request && $request->isMethod('POST')) {
            $user = new User();
            $email = $request->request->get('email');
            $password = $request->request->get('password');
            // $repository = $this->getDoctrine()->getRepository(User::class);
            $product = $this->userRepository->findOneBy([
                'email' => $email,
                'password' => md5($password),
            ]);

            $api = $this->apiRepository->findOneBy([
                'name' => 'OrderSSL'
            ]);

            // dd($api);

            if($product) {
                // echo 'Login succeed'; die;
                $message_type = 'success';
                $response = new Response(json_encode(["url" => $api->url]));
                return $response;
            }
            else {
                // echo 'Login fail'; die;
                $message_type = 'danger';
                // $response = new Response(json_encode('Login fail'));
                $response = new Response(json_encode(["res" => $api->getUrl()]));
                return $response;
            }
        }

        $this->session->set('message_type', $message_type);
        return $this->redirectToRoute('auth');
    }

    /**
     * @Route("/auth/register", name="auth_register")
     */
    public function register()
    {
        // echo 'register';
        return $this->render('auth/register.html.twig', [
            'controller_name' => 'OrderController',
        ]);
    }

    /**
     * @Route("/auth/registerAction", name="auth_register_action")
     */
    public function registerAction(Request $request)
    {
        if($request && $request->isMethod('POST')) {
            $user = new User();
            // $this->createForm(User::class, $user);
            $user->setName($request->request->get('name'));
            $user->setEmail($request->request->get('email'));
            $user->setPassword(md5($request->request->get('password')));
            $user->setCreateAt(new DateTime());
            $user->setUpdateAt(new DateTime());
            $user->setRole("USER");

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('auth');
    }
}
