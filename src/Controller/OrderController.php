<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Repository\ApiRepository;
use App\Repository\UserRepository;

use App\Entity\User;
use DateTime;


class OrderController extends AbstractController
{
    private $session;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var ApiRepository
     */
    private $apiRepository;

    /**
     * ProductReviewController constructor.
     *
     * @param UserRepository $userRepository
     * @param ApiRepository $apiRepository
     */
    public function __construct(
        SessionInterface $session,
        UserRepository $userRepository,
        ApiRepository $apiRepository
    )
    {
        $this->session = $session;
        $this->userRepository = $userRepository;
        $this->apiRepository = $apiRepository;
    }

    /**
     * @Route("/order", name="order")
     */
    public function index()
    {   
        $message_type = $this->session->get('message_type');
        $message_type = $this->session->remove('message_type');
        return $this->render('order/index.html.twig', [
            'controller_name' => 'OrderController',
            'message_type' => $message_type,
        ]);
    }

    /**
     * @Route("/order/login", name="order_login")
     */
    public function login()
    {
        return $this->render('order/buy.html.twig', [
            'controller_name' => 'OrderController',
        ]);
    }

    /**
     * @Route("/order/loginAction", methods={"POST"}, name="order_login_action")
     */
    public function loginAction(Request $request)
    {
        $message_type = null;
        if($request && $request->isMethod('POST')) {
            $domain = $request->request->get('domain');
            $api = $this->apiRepository->findOneBy([
                'name' => 'OrderSSL'
            ]);

            if($api) {
                // echo 'Login succeed'; die;
                $message_type = 'success';
                $response = new Response(
                    json_encode([
                        "url" => $api->getUrl(),
                        "domain" => $domain
                    ])
                );
                return $response;
            }
            else {
                // echo 'Login fail'; die;
                $message_type = 'danger';
                // $response = new Response(json_encode('Login fail'));
                $response = new Response(json_encode(["domain" => $domain]));
                return $response;
            }
        }

        $this->session->set('message_type', $message_type);
        return $this->redirectToRoute('order');
    }

    /**
     * @Route("/order/register", name="order_register")
     */
    public function register()
    {
        // echo 'register';
        return $this->render('order/register.html.twig', [
            'controller_name' => 'OrderController',
        ]);
    }

    /**
     * @Route("/order/registerAction", name="order_register_action")
     */
    public function registerAction(Request $request)
    {
        if($request && $request->isMethod('POST')) {
            $user = new User();
            // $this->createForm(User::class, $user);
            $user->setName($request->request->get('name'));
            $user->setEmail($request->request->get('email'));
            $user->setPassword(md5($request->request->get('password')));
            $user->setCreateAt(new DateTime());
            $user->setUpdateAt(new DateTime());
            $user->setRole("USER");

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('order');
    }
}
