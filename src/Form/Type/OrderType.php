<?php
namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        // $resolver->setDefaults([
        //     // ...,
        //     'require_due_date' => false,
        // ]);

        // // you can also define the allowed types, allowed values and
        // // any other feature supported by the OptionsResolver component
        // $resolver->setAllowedTypes('require_due_date', 'bool');
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('name')->add('description')->add('save', SubmitType::class);
    }
}